#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/jiffies.h>

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Module for writing jiffies");
MODULE_AUTHOR("FM");

int mojmodul_init(void){
	printk(KERN_INFO "Pocetak: %lu %d\n", jiffies/HZ, HZ);
	return 0;
}

void mojmodul_exit(void){
	printk(KERN_INFO "Kraj: %lu %d\n", jiffies/HZ, HZ);
}

module_init(mojmodul_init);
module_exit(mojmodul_exit);
