#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Module for showing process information");
MODULE_AUTHOR("FM");

int sviprocesi_init(void){
	struct task_struct *p;

	printk("POCETAK: Ispis svih procesa:\n");
	for_each_process(p){
		printk("Task %s (pid = %d)(state = %ld) (Real parent name = %s) (Parent name = %s)\n"
				, p->comm, task_pid_nr(p), p->state, p->real_parent->comm, p->parent->comm);
	}
	return 0;
}

void sviprocesi_exit(void){
	//struct task_struct *p;

	printk("KRAJ\n");
	//printk("KRAJ: Ispis svih procesa:\n");
	/*for_each_process(p){
		printk("Task %s (pid = %d) (Real parent name = %s) (Parent name = %s)\n"
				, p->comm, task_pid_nr(p), p->real_parent->comm, p->parent->comm);
	}*/
}

module_init(sviprocesi_init);
module_exit(sviprocesi_exit);
